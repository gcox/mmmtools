#include <VirtualRobot/RuntimeEnvironment.h>

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <map>
#include <iostream>
#include <fstream>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <MMM/Motion/MotionReaderXML.h>

#include <boost/algorithm/string.hpp>

#include <MMM/Motion/Motion.h>
#include <MMM/Model/Model.h>

#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include <MMM/Motion/MarkerMotion.h>
#include <MMM/Motion/MarkerData.h>
#include <MMMSimoxTools/MMMSimoxTools.h>
#include <MMM/XMLTools.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <VirtualRobot/Dynamics/dynamics.h>
#include <VirtualRobot/XML/RobotIO.h>
#include "MMMInverseDynamicsConfiguration.h"

#include "boost/foreach.hpp"



using std::cout;
using std::endl;


class JointTorques : public MMM::MotionFrameEntry
{
public:
    JointTorques() : MotionFrameEntry("JointTorque")
    {
        s = "not set";
    }

    virtual std::string toXML()
    {
        std::string tab = "\t";
        std::stringstream res;
        res << tab << tab << "<" << tagName << ">";
        for (int i=0; i < jointTorquesVector.rows(); i++)
        {
            res << jointTorquesVector[i] << " ";
        }
        res << "</" << tagName << ">" << endl;
        return res.str();
    }


    void setJointTorques(Eigen::VectorXf torques)
    {
        jointTorquesVector = torques;
    }
    std::string s;

private:

    Eigen::VectorXf jointTorquesVector;
};
typedef boost::shared_ptr<JointTorques> JointTorquesPtr;

std::map<std::string,int> generateJointMapping(MMM::MotionPtr motion)
{
    std::map<std::string, int> jointMapping;
    std::vector<std::string> motionNames =  motion->getJointNames();

    for (int i=0; i < motionNames.size(); i++)
    {
        jointMapping[motionNames[i]] = i;
    }
    return jointMapping;
}

Eigen::VectorXf permuteJointVector(Eigen::VectorXf joints, VirtualRobot::RobotNodeSetPtr nodeSet, std::map<std::string,int> mapping)
{
    Eigen::VectorXf permutedJoints = Eigen::VectorXf::Zero(nodeSet->getAllRobotNodes().size());

    for (int i=0; i < nodeSet->getAllRobotNodes().size(); i++)
    {
        permutedJoints[i] = joints[mapping[nodeSet->getNode(i)->getName()]];
    }

    return permutedJoints;


}

Eigen::VectorXf unpermuteJointVector(Eigen::VectorXf joints, VirtualRobot::RobotNodeSetPtr nodeSet, std::map<std::string, int> mapping)
{
    std::vector<VirtualRobot::RobotNodePtr> nodes = nodeSet->getAllRobotNodes();
    Eigen::VectorXf unpermutedJoints = Eigen::VectorXf::Zero(nodes.size());

    for(int i=0; i < nodes.size(); i++)
    {
        unpermutedJoints[mapping[nodes[i]->getName()]] = joints[i];
    }

    return unpermutedJoints;

}




int main(int argc, char *argv[])
{

    // first, initialize Config struct
    MMMInverseDynamicsConfiguration config;

    if(!config.processCommandLine(argc, argv))
    {
        cout << "Failed to process command line, aborting" << endl;
        return -1;
    }

    // set up model processor
    MMM::ModelProcessorPtr modelProcessor;

    if(!config.modelProcessorConfig.empty())
    {
        modelProcessor = MMM::ModelProcessorFactory::fromConfigFile(config.modelProcessorConfig);

        if(!modelProcessor)
        {
            cout << "Failed to create ModelProcessor!" << endl;
            return -1;
        }
        cout << "Setting up model processor....ok!" << endl;
    }

    // load normalized model and scale it
    MMM::ModelPtr normalizedModel;
    MMM::ModelPtr scaledModel;
    try
    {
        MMM::ModelReaderXMLPtr xmlReader(new MMM::ModelReaderXML());
        normalizedModel = xmlReader->loadModel(config.model);

        cout << "Processing normalized model" << endl;

        scaledModel = modelProcessor->convertModel(normalizedModel);
        cout << "Model processed!" << endl;
    }
    catch(VirtualRobot::VirtualRobotException &e)
    {
        cout << "Error while reading robot from file" << endl;
        cout << e.what();
        return -1;
    }


    // build RBDL model
    VirtualRobot::RobotPtr scaledRobot = MMM::SimoxTools::buildModel(scaledModel,false);

    VirtualRobot::RobotNodeSetPtr nodeSet = scaledRobot->getRobotNodeSet(config.nodeSetName);

    VirtualRobot::Dynamics dynamicsProcessor = VirtualRobot::Dynamics(nodeSet);


    // load motion
    MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML());
    std::vector<std::string> motionNames = motionReader->getMotionNames(config.motionFile);

    if(motionNames.size() == 0)
    {
        MMM_ERROR << "Motion file contains no motions" << endl;
        return -1;
    }

    BOOST_FOREACH(std::string motionName, motionNames)
    {
        MMM::MotionPtr motion = motionReader->loadMotion(config.motionFile,motionName);
        if(!motion)
            continue;
        cout << "Loading motion: " << motionName << endl;
        //std::map<std::string, int> jointMapping = generateJointMapping(motion);
        std::map<std::string, int> jointMapping = generateJointMapping(motion);

        std::vector<MMM::MotionFramePtr> motionFrames = motion->getMotionFrames();

        BOOST_FOREACH(MMM::MotionFramePtr motionFrame, motionFrames)
        {
            Eigen::VectorXf q = permuteJointVector(motionFrame->joint,nodeSet,jointMapping);
            Eigen::VectorXf qdot = permuteJointVector(motionFrame->joint_vel,nodeSet,jointMapping);
            Eigen::VectorXf qddot = permuteJointVector(motionFrame->joint_acc,nodeSet,jointMapping);

            Eigen::VectorXd tau = dynamicsProcessor.getInverseDynamics(q.cast<double>(),qdot.cast<double>(),qddot.cast<double>());

            JointTorquesPtr torquesPtr = boost::shared_ptr<JointTorques>(new JointTorques());
            torquesPtr->setJointTorques(unpermuteJointVector(tau.cast<float>(),nodeSet,jointMapping));

            motionFrame->addEntry("JointTorques",torquesPtr);


        }

    std::string motionXML = motion->toXML();
    std::string filename = config.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_ID.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        MMM_ERROR << " Could not write to file " << filename_completed << endl;
    }

    //MMM::XML::saveXML("test.xml", motion->toXML());


    }

}
