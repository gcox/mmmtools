/*
 * MatrixOperation.h
 *
 *  Created on: 10.11.2009
 *  Author: 	Martin Do
 *	Copyright: 	Martin Do, Chair Prof. Dillmann (IAIM),
 *            	Institute for Anthropomatics (IFA),
 *		        Karlsruhe Institute of Technology (KIT). All rights reserved.
 *
 *	Changes:
 *
 *	Description:
 */


#ifndef MATRIXOPERATION_H_
#define MATRIXOPERATION_H_

#include <vector>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gslwrap/matrix_double.h>
#include <gslwrap/matrix_vector_operators.h>


using namespace gsl;

class CMatrixOperation
{
public:

	static matrix getMatrixFromData(std::vector<gsl::vector> dataSet);
	static std::vector<gsl::vector> getDataFromMatrix(matrix reducedDataMatrix);
	static gsl::vector getMeanVector(matrix dataMatrix);
	static matrix getCovarianceMatrix(matrix dataMatrix, gsl::vector means);
	static matrix getCorrelationMatrix(matrix dataMatrix, gsl::vector means);
	static matrix getEigenValues(matrix inputMatrix, bool sortDescening);
	static gsl::vector postMultiply(matrix* dataMatrix, gsl::vector* inputVector);
	static gsl::vector preMultiply(gsl::vector* inputVector, matrix* dataMatrix);
	static bool isOrthogonal(matrix* dataMatrix);
	static bool allEqual(matrix* dataMatrix, double value);
	static bool isSymmetric(matrix* dataMatrix);
	static void print(matrix* dataMatrix);

private:
};

#endif /* MATRIXOPERATION_H_ */
