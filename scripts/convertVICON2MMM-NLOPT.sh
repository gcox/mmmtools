#!/bin/sh
MMMConverterGUI \
	--inputData $1 \
	--converterConfigFile "${MMMTools_DIR}/data/NloptConverterVicon2MMM_WinterConfig.xml" \
	--converter "NloptConverterVicon2MMM" \
	--sourceModel "" \
	--sourceModelProcessor "" \
	--sourceModelProcessorConfigFile "" \	
	--targetModel "${MMMTools_DIR}/data/Model/Winter/mmm.xml" \
	--targetModelProcessor "Winter" \
	--targetModelProcessorConfigFile "${MMMTools_DIR}/data/ModelProcessor_Winter_1.79.xml"
	

