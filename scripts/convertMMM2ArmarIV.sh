#!/bin/sh
MMMConverterGUI \
	--inputData $1 \
	--converterConfigFile "${MMMTools_DIR}/data/ConverterMMM2Armar4_Config.xml" \
	--converter "ConverterMMM2Robot" \
	--sourceModel "${MMMTools_DIR}/data/Model/Winter/mmm.xml" \
	--sourceModelProcessor "Winter" \
	--sourceModelProcessorConfigFile "${MMMTools_DIR}/data/ModelProcessor_Winter_1.79.xml" \
	--targetModel "${MMMTools_DIR}/data/Model/Armar4/Armar4.xml" \
	--targetModelProcessor "" \
	--targetModelProcessorConfigFile "" 
